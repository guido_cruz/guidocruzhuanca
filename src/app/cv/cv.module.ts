import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutMeComponent } from './about-me/about-me.component';
import { CvComponent } from './cv.component';

@NgModule({
  declarations: [AboutMeComponent, CvComponent],
  imports: [
    CommonModule
  ],
  exports: [CvComponent]
})
export class CvModule { }
