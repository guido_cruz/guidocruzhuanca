import { Component } from '@angular/core';

@Component({
  selector: 'guido-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'guidoCruzHuanca';
}
